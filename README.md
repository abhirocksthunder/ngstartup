# Ngstartup

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## bootstrap site for reference 

https://getbootstrap.com/docs/4.0/components/alerts/

## components created using following scripts

D:\training\ngstartup>ng g c day1/auto
CREATE src/app/day1/auto/auto.component.html (19 bytes)
CREATE src/app/day1/auto/auto.component.spec.ts (614 bytes)
CREATE src/app/day1/auto/auto.component.ts (268 bytes)
CREATE src/app/day1/auto/auto.component.scss (0 bytes)
UPDATE src/app/app.module.ts (472 bytes)       

D:\training\ngstartup>ng g c day1/manual       
CREATE src/app/day1/manual/manual.component.html (21 bytes)
CREATE src/app/day1/manual/manual.component.spec.ts (628 bytes)
CREATE src/app/day1/manual/manual.component.ts 
(276 bytes)
CREATE src/app/day1/manual/manual.component.scss (0 bytes)
UPDATE src/app/app.module.ts (559 bytes)       

D:\training\ngstartup>ng g c day2/manual       
CREATE src/app/day2/manual/manual.component.html (21 bytes)
CREATE src/app/day2/manual/manual.component.spec.ts (628 bytes)
CREATE src/app/day2/manual/manual.component.ts 
(276 bytes)
CREATE src/app/day2/manual/manual.component.scss (0 bytes)

D:\training\ngstartup>ng g c day2/auto
CREATE src/app/day2/auto/auto.component.html (19 bytes)
CREATE src/app/day2/auto/auto.component.spec.ts (614 bytes)
CREATE src/app/day2/auto/auto.component.ts (268 bytes)
CREATE src/app/day2/auto/auto.component.scss (0 bytes)

D:\training\ngstartup>ng g c shared/header     
CREATE src/app/shared/header/header.component.html (21 bytes)
CREATE src/app/shared/header/header.component.spec.ts (628 bytes)
CREATE src/app/shared/header/header.component.ts (276 bytes)
CREATE src/app/shared/header/header.component.scss (0 bytes)
UPDATE src/app/app.module.ts (648 bytes)       

D:\training\ngstartup>ng g c _shared/header    
CREATE src/app/_shared/header/header.component.html (21 bytes)
CREATE src/app/_shared/header/header.component.spec.ts (628 bytes)
CREATE src/app/_shared/header/header.component.ts (276 bytes)
CREATE src/app/_shared/header/header.component.scss (0 bytes)

D:\training\ngstartup>ng g c _shared/footer
CREATE src/app/_shared/footer/footer.component.html (21 bytes)
CREATE src/app/_shared/footer/footer.component.spec.ts (628 bytes)
CREATE src/app/_shared/footer/footer.component.ts (276 bytes)
CREATE src/app/_shared/footer/footer.component.scss (0 bytes)
UPDATE src/app/app.module.ts (738 bytes)
