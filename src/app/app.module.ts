import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutoComponent } from './day1/auto/auto.component';
import { Day1ManualComponent } from './day1/manual/day1-manual.component';
import { HeaderComponent } from './_shared/header/header.component';
import { FooterComponent } from './_shared/footer/footer.component';
import { Day2Component } from './day2/day2.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Day1Component } from './day1/day1.component';

@NgModule({
  declarations: [
    AppComponent,
    AutoComponent,
    Day1ManualComponent,
    HeaderComponent,
    FooterComponent,
    Day2Component,
    DashboardComponent,
    Day1Component,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
