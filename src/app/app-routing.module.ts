import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { Day1Component } from "./day1/day1.component";
import { Day2Component } from "./day2/day2.component";

const routes: Routes = [
  {
    path:'',
    pathMatch: 'full',
    component: DashboardComponent
  },
  {
    path:'day1',
    component: Day1Component
  },
  {
    path:'day2',
    component: Day2Component
  },
  {
    path:'**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
